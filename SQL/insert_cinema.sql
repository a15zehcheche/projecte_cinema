#insert
USE CINEMA;

#sala
insert into sala values(1,120,6);

#pelicula
insert into pelicula values("Cadena perpètua","img/shawns.jpg",6.00);
insert into pelicula values("El Padrino","img/thegodfather.jpg",6.00);
insert into pelicula values("El Caballero Oscuro","img/batman.jpg",6.00);
insert into pelicula values("Fight Club","img/fightclub.jpg",6.00);
insert into pelicula values("La Llista de Schindler","img/schindlers.jpg",6.00);
insert into pelicula values("El Señor de los anillos: El Retorno del Rey","img/senordelosanillos.jpg",6.00);
insert into pelicula values("Gladiator","img/gladiator.jpg",6.00);
insert into pelicula values("Vengadores: Infinity War","img/avengers.jpg",6.00);
insert into pelicula values("Alien, el octavo pasajero","img/alien.jpg",6.00);
insert into pelicula values("Braveheart","img/braveheart.jpg",6.00);
insert into pelicula values("Toy Story(Juguetes)","img/toystory.jpg",6.00);
insert into pelicula values("Blade Runner","img/blade.jpg",6.00);
insert into pelicula values("Regreso al futuro","img/futuro.jpg",6.00);
insert into pelicula values("Up","img/up.jpg",6.00);
insert into pelicula values("Django desencadenado","img/django.jpg",6.00);
insert into pelicula values("Harry Potter y las reliquias de la muerte: Parte 2","img/harryreliquia2.jpg",6.00);
insert into pelicula values("Logan","img/logan.jpg",6.00);
insert into pelicula values("La naranja mecánica","img/naranja.jpg",6.00);
insert into pelicula values("La Bella y la Bestia","img/bella.jpg",6.00);
insert into pelicula values("Marvel Los Vengadores","img/marvel.jpg",6.00);
insert into pelicula values("American History X","img/american.jpg",6.00);
insert into pelicula values("Warcraft:El origen","img/warcraft.jpg",6.00);



#seccio

insert into seccio  values (5,"2018/12/14","18:00:00",1,"El Padrino");
insert into seccio  values (6,"2018/12/15","18:00:00",1,"La Llista de Schindler");
insert into seccio  values (7,"2018/12/16","20:00:00",1,"El Caballero Oscuro");
insert into seccio  values (8,"2018/12/17","18:00:00",1,"Fight Club");
insert into seccio  values (9,"2018/12/18","18:00:00",1,"La Llista de Schindler");
insert into seccio  values (10,"2018/12/19","18:00:00",1,"Cadena perpètua");
insert into seccio  values (1,"2018/12/20","18:00:00",1,"Cadena perpètua");
insert into seccio  values (2,"2018/12/21","20:00:00",1,"Fight Club");
insert into seccio  values (3,"2018/12/22","18:00:00",1,"El Padrino");
insert into seccio  values (4,"2018/12/23","20:00:00",1,"El Caballero Oscuro");
insert into seccio  values (11,"2018/12/24","16:00:00",1,"Gladiator");
insert into seccio  values (12,"2018/12/25","20:00:00",1,"Vengadores: Infinity War");
insert into seccio  values (13,"2018/12/26","18:00:00",1,"Alien, el octavo pasajero");
insert into seccio  values (14,"2018/12/27","16:00:00",1,"Braveheart");
insert into seccio  values (15,"2018/12/28","18:00:00",1,"Toy Story(Juguetes)");
insert into seccio  values (16,"2018/12/29","18:00:00",1,"Blade Runner");
insert into seccio  values (17,"2018/12/30","20:00:00",1,"Toy Story(Juguetes)");
insert into seccio  values (18,"2018/12/31","16:00:00",1,"Regreso al futuro");
insert into seccio  values (19,"2019/1/1","20:00:00",1,"Up");
insert into seccio  values (20,"2019/1/2","18:00:00",1,"Django desencadenado");
insert into seccio  values (21,"2019/1/3","20:00:00",1,"Harry Potter y las reliquias de la muerte: Parte 2");
insert into seccio  values (22,"2019/1/4","18:00:00",1,"Logan");
insert into seccio  values (23,"2019/1/5","20:00:00",1,"La naranja mecánica");
insert into seccio  values (24,"2019/1/6","16:00:00",1,"La Bella y la Bestia");
insert into seccio  values (25,"2019/1/7","18:00:00",1,"Marvel Los Vengadores");
insert into seccio  values (26,"2019/1/8","20:00:00",1,"American History X");
insert into seccio  values (27,"2019/1/9","16:00:00",1,"Warcraft:El origen");
insert into seccio  values (28,"2019/1/10","16:00:00",1,"La Llista de Schindler");
insert into seccio  values (29,"2019/1/11","20:00:00",1,"Cadena perpètua");
insert into seccio  values (30,"2019/1/12","18:00:00",1,"El Caballero Oscuro");
insert into seccio  values (31,"2019/1/13","16:00:00",1,"La Llista de Schindler");
insert into seccio  values (32,"2019/1/14","18:00:00",1,"Warcraft:El origen");
insert into seccio  values (33,"2019/1/15","20:00:00",1,"Cadena perpètua");
insert into seccio  values (34,"2019/1/16","18:00:00",1,"El Caballero Oscuro");
insert into seccio  values (35,"2019/1/17","16:00:00",1,"Cadena perpètua");

#user
insert into usuari values("a15zehcheche@iam.cat");
insert into usuari values("a15weixuxxux@iam.cat");
insert into usuari values("ana@iam.cat");
insert into usuari values("joan@iam.cat");

#entrada 
insert into entrada values("10001","a15zehcheche@iam.cat",5,"2018/12/15",54.00);
insert into entrada values("10002","a15weixuxxux@iam.cat",3,"2018/12/22",54.00);
insert into entrada values("10003","ana@iam.cat",7,"2018/12/16",54.00);
insert into entrada values("10004","joan@iam.cat",4,"2018/12/23",54.00);


#Butaques reservat dels burtaques
insert into butaquesSeleccionat values(10001,30);
insert into butaquesSeleccionat values(10001,31);
insert into butaquesSeleccionat values(10001,32);

insert into butaquesSeleccionat values(10002,76);
insert into butaquesSeleccionat values(10002,77);
insert into butaquesSeleccionat values(10002,78);

insert into butaquesSeleccionat values(10003,65);
insert into butaquesSeleccionat values(10003,66);
insert into butaquesSeleccionat values(10003,67);

insert into butaquesSeleccionat values(10004,53);
insert into butaquesSeleccionat values(10004,54);
insert into butaquesSeleccionat values(10004,55);


