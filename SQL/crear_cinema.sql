create database CINEMA CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE CINEMA;
create table if not exists pelicula(
	nomPeli varchar(128) primary key ,
    imgUrl varchar(128),
    preu_base decimal(4,2)
);

create table if not exists sala(
	idSala int primary key,
    numButaques int,
	filaVip smallint
);

create table if not exists seccio(
	idSeccio int auto_increment primary key ,
    fecha date,
    hora time,
    idSala int,
    nomPeli varchar(128),
    foreign key (idSala) references sala(idSala)
    on delete restrict on update cascade,
    foreign key (nomPeli) references pelicula(nomPeli)
    on delete restrict on update cascade
);

create table if not exists usuari(
	email varchar(128) primary key,
	nom varchar(128) DEFAULT null,
	cognom varchar(128) DEFAULT null,
	telefon int(9) DEFAULT null 
);

create table if not exists entrada(
	codi_entrada int primary key,
	email varchar(128),
    idSeccio int,
	fecha date,
    preu_total decimal(5,2),
	foreign key (email) references usuari(email)
    on delete restrict on update cascade,
    foreign key (idSeccio) references seccio(idSeccio)
    on delete restrict on update cascade

);

create table if not exists butaquesSeleccionat(
	codi_entrada int,
    num_butaques int,
    primary key(codi_entrada,num_butaques),
    foreign key (codi_entrada) references entrada(codi_entrada)
    on delete restrict on update cascade
);





