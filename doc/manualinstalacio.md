# Manual d'instalació
***
### PART CLONACIÓ DE DADES
Per instal·lar la pagina l'hem de penjar al labs.
Ens hem de conectar al labs via SSH. I accedir a la carpeta propìetaria, un cop alla anem a la capreta public_html.
Agafem la linia URL del bit bucket que comença per "git clone..." i la inserim al terminal per tal de clonar la pagina a la carpeta del labs.
A continuació per a que la pagina es pugui veure des de el labs hem de posar permisos. per tant introduirem el seguent: chmod 755 -R "nom de la carpeta".

### PART BASE DE DADES
Per inserir la base de dades hem de seguir els següents pasos:
1. Hem de anar a https://labs.iam.cat:56000/login/ fem login.
2. En l'apartat de DB, fem un clic en el símbol + i introduïm el que es demena (nom de DB,password,localhost...)
3. En http://labs.iam.cat/phpmyadmin/ fem login de la DB que acabem de crear.
4. Dins del **phpmyadmin** hem de anar a l'apartat de SQL
5. I nomes tenim que copiar el codi que esta dins de SQL/crear_cinema.sql (que el hem clonat del bitbucket) i executar.
6. Fem el mateix amb el SQL/insert_cinema.sql que servira per introduïr les dades basiques.
7. Amb tot aixo la pagina ja funciona.