# PROBLEMES TROBADES

### CONNEXIO SERVIDOR MYSQL
* El servidor mysql només te el port 3306 obert per el localhost.
> Modificar el fitxer /etc/mysql/mysql.conf.d/mysqld.cnf
> Canviar la linea de bind-address = 172.0.0.1 per bind-address = *
> Finalment reiniciar el servei de mysql
* El usuari ROOT només es pot connectar per localhost.
> Crear un usuari de mysql per tot la xarxa.
> CREATE USER 'nom_usuari'@'%' IDENTIFIED BY 'password';
> GRANT ALL PRIVILEGES ON * . * TO 'nom_usuari'@'%';

