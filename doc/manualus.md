# Cinema
>la pagina consta de:
-Home
-Consulta
-Buscar
-Pel·licula
-Butaques
-Admin

## Home
> L'apartat home es el primer que veuras al entrar al lloc web. El primer que es veu es un destacat amb les pelicules recomanades. I mes avall les pelicules que hi ha durant els propers dies. 

> Clicant sobre una pelicula seras redirigit a la pagina d'aquesta on podras veure el titol, preu i un boto que et permetra anar a la seleccio de butaques. clica'l si vols reservar!

## Butaques
> En aquest apartat pots seleccionar la o les butaques que vols reservar. nomes cal clicar sobre aquella butaca que vulguis i en cas de que no estigui ocupada a la teva dreta podras veure la informacio de la teva compra. (numero de butaca, tipus de la butaca i el preu total) Clica el boto comprar quan estiguis llest!

## Compra
>Un cop has triat les butaques que vols, et portem a la part de registre per reservar la entrada. Nomes hauras d'introduir el email i guardaras la teva entrada. Un cop posat el email clica comprar i veuras el teu codi d'entrada.
>mes a baix hi ha un boto per a consultar la entrada clica'l. Podras veure el resum de la teva compra.

## Consulta
>>Introduint el teu email pots veure un resum de la compra total. Constara de les parts següents:
- Codi de l'entrada
- Titol de la pelicula
- Data i hora de la pelicula
- Numero de la sala
- Numero de butaques(amb el numero i tipus de butaca)
- Preu total

## Buscar
>> Si vols buscar una peli en concret doncs en aquest apartat pots buscar la pelicula introduint el nom sencer de la pelicula o be si no el recordes tot, pots escriure una paraula o lletra i veuras tots els resultats que coincideixin amb la teva busqueda. 
>> Desde aqui podras veure el titol i preu de la pelicla i el boto per reservar la pelicula que et redirigira a la pagina pelicula on pdras escollir el dia que voldras anar i tornaras al proces de seleccio i compra d'entrades.

## Admin
>Finalment la part d'admin,per poder accedir t'hauras de loguejar 
usuari: ausias
contra:ausias
>Des de aqui podras veure les dades del cinema es a dir el numero d'entrades venudes, el numero de diners guanyats. El que veuras al entrar seran les dades del dia d'avui.
>Per buscar un dia en concret ves a "buscar" i clica el dia sobre el qual vulguis veure les dades i estadistiques d'aquell dia.

