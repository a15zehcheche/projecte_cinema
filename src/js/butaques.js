$(document).ready(function () {

    //Funcions---------------------------------------------------------------------------------------

    function DibujarTabla(vip,butacas_ocupats){
        let butaques = `<table>`; 
        for(let i=12;i>0;i--){
            butaques += `<tr>`;
            for(let j=0;j<=9;j++){
                let ocupat =0;
                for(let l = 0; l<butacas_ocupats.length;l++){
                    if(butacas_ocupats[l]==`${i}${j}`){
                        ocupat=1;
                    }
                }
                if(i==vip){
                    if(ocupat)
                    butaques += `<td num="${i}${j}" class="ocupat"><img src='../img/ocupat.png'></td>`;
                    else
                    butaques += `<td num="${i}${j}"><img src='../img/lliurevip.png'></td>`;
                }
                else{
                    if(ocupat)
                    butaques += `<td num="${i}${j}" class="ocupat"><img src='../img/ocupat.png'></td>`;
                    else
                    butaques += `<td num="${i}${j}"><img src='../img/disponible.png'></td>`;
                }
               
            }
            butaques += `</tr>`;
        }
        butaques += `<tr><th colspan="13" class="pantalla"><img src="../img/pantalla_abajo.png"></img></th></tr>`;
        butaques += `</table>`;
        //butaques += `<div  class="pantalla"><img src="img/pantalla_abajo.png"></img><div>`;
        return butaques;
    }


    //Agafar Dades que venen de PHP-------------------------------------------------------------------
    
    let error = parseInt($(".error").text());
    let preu = parseInt($(".preu").text());
    let filaVip = parseInt($(".fila").text());
    $(".no_display").hide();
    let str_butaques = $.trim($(".butacas_ocupat").text());
    let butacas_ocupats = str_butaques.split(" ");
    let data = $(".date").attr("value");
    //Variables---------------------------------------------------------------------------------------
    var butcont=0;
    var listbut = [];
    let preu_vip = preu+2;
    let vips = 0;
    let normals = 0;
    let preu_total;
    let dia_semana = new Date(data).getDay();
    let historial = [];
    
    //INICI-------------------------------------------------------------------------------------------
    if(error == 1){
        $(".titol").html("ERROR");
        $(".content").hide();
        Swal({
            type: 'error',
            title: 'No hi ha data o nom de pelicula',
            footer: '<a href="../index.php">Torna en darrera.</a>'
          })
    }else{
        $(".sala").html(DibujarTabla(filaVip,butacas_ocupats));
        $(".quant_vips").html(vips);
        $(".quant_normal").html(normals);
        $(".titol").append(`: ${data}`);
        if(dia_semana == 3){
            $(".espectador").html("<p><span class='letter_dia'>Dia </span><span class='letter_espec'>Espectectador!</span></p>");
        }
        //QUAN FA UN CLICK EN UNA BUTACA
        $("td").click(function(){
            let butacas_select;
            if($(this).hasClass("ocupat")){
                let timerInterval
                    swal({
                    type: 'error',
                    title: 'Ooopss.....',
                    html: 'Esta Ocupat, selecciona un altre.',
                    timer: 1500,
                    onOpen: () => {
                        swal.showLoading()
                        timerInterval = setInterval(() => {
                        swal.getContent().querySelector('strong')
                            .textContent = swal.getTimerLeft()
                        }, 100)
                    },
                    onClose: () => {
                        clearInterval(timerInterval)
                    }
                    }).then((result) => {
                    if (
                        // Read more about handling dismissals
                        result.dismiss === swal.DismissReason.timer
                    ) {
                        console.log('Alert tancament per temps.')
                    }
                    })
            }
            else{
                //SI ESTA SELECCIONAT EL DESMARCA
                if($(this).hasClass("assig")){
                    $(this).removeClass("assig")
                    var index = listbut.indexOf($(this).attr("num"))
                    if (index > -1) {
                        listbut.splice(index, 1)
                    }
                    butcont--;
                    
                    $(".seleccionats").attr("value",`${listbut}`);
                    if($(this).attr("num").startsWith(filaVip)){
                        $(this).html("<img src='../img/lliurevip.png'>")
                        $(".quant_vips").html(--vips);
                        $(".euro_vips").html(preu_vip*vips);
                        preu_total= parseInt(preu_vip*vips)+parseInt(preu*normals);
                        $(".preu_total").attr("value",`${preu_total}`);
                        $(".preu_total").html(preu_total);
                        butacas_select = `<p class="but_mos"><span><img src='../img/lliurevip_p.png'> Butaca: ${$(this).attr("num")} </span></p>`;
                    }
                    else{
                        $(this).html("<img src='../img/disponible.png'>")
                        $(".quant_normal").html(--normals);
                        $(".euro_normal").html(preu*normals);
                        preu_total= parseInt(preu_vip*vips)+parseInt(preu*normals);
                        $(".preu_total").attr("value",`${preu_total}`);
                        $(".preu_total").html(preu_total);
                        butacas_select = `<p class="but_mos"><span><img src='../img/disponible_p.png'> Butaca: ${$(this).attr("num")} </span></p>`;
                    }
                    var index2 = historial.indexOf(butacas_select)
                    if (index2 > -1) {
                        historial.splice(index2, 1)
                    }
                }
    
                //SI TE 10 SELECCIONAT DONA ERROR, SI ESTA DESMARCAT EL SELECCIONA
                else{
                    if(butcont==10){
                        let timerInterval
                        swal({
                        type: 'warning',
                        title: 'Ooopss.....',
                        html: 'Nomes pots seleccionar 10 butaques.',
                        timer: 1500,
                        onOpen: () => {
                            swal.showLoading()
                            timerInterval = setInterval(() => {
                            swal.getContent().querySelector('strong')
                                .textContent = swal.getTimerLeft()
                            }, 100)
                        },
                        onClose: () => {
                            clearInterval(timerInterval)
                        }
                        }).then((result) => {
                        if (
                            // Read more about handling dismissals
                            result.dismiss === swal.DismissReason.timer
                        ) {
                            console.log('Alert tancament per temps.')
                        }
                        })
                    }
                    else{
                        $(this).html("<img src='../img/escollida.png'>")
                        $(this).addClass("assig")
                        butcont++;
                        listbut.push($(this).attr("num"))
                        $(".seleccionats").attr("value",`${listbut}`);
                        if($(this).attr("num").startsWith(filaVip)){
                            $(".quant_vips").html(++vips);
                            $(".euro_vips").html(preu_vip*vips);
                            preu_total= parseInt(preu_vip*vips)+parseInt(preu*normals);
                            $(".preu_total").attr("value",`${preu_total}`);
                            $(".preu_total").html(preu_total);
                            butacas_select = `<p class="but_mos"><span><img src='../img/lliurevip_p.png'> Butaca: ${$(this).attr("num")} </span></p>`;
                        }
                        else{
                            $(".quant_normal").html(++normals);
                            $(".euro_normal").html(preu*normals);
                            preu_total= parseInt(preu_vip*vips)+parseInt(preu*normals);
                            $(".preu_total").attr("value",`${preu_total}`);
                            $(".preu_total").html(preu_total);
                            butacas_select = `<p class="but_mos"><span><img src='../img/disponible_p.png'> Butaca: ${$(this).attr("num")} </span></p>`;
                        }
                        historial.push(butacas_select);
                    }
                }
                $(".display_butaques_select").html(historial);
            }
        })
    
        $(".submit").click(function (e) {
                    
            if(butcont==0){
                let timerInterval
                Swal({
                type:'warning',
                title: 'Opsss......',
                html: 'Tens que seleccionar com a minim 1.',
                timer: 1000,
                onBeforeOpen: () => {
                    Swal.showLoading()
                    timerInterval = setInterval(() => {
                    Swal.getContent().querySelector('strong')
                        .textContent = Swal.getTimerLeft()
                    }, 100)
                },
                onClose: () => {
                    clearInterval(timerInterval)
                }
                }).then((result) => {
                if (
                    // Read more about handling dismissals
                    result.dismiss === Swal.DismissReason.timer
                ) {
                    console.log('I was closed by the timer')
                }
                })
                e.preventDefault(e);
            }
        });
    }
})