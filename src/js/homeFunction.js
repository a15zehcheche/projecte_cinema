
$(document).ready(function () {
    show_img = (function () {
        let show_num = 0;
        let numbre_imgs = 3;
        function shwo_img() {
            $(".img_box").hide();
            $(".img_box").eq(show_num).show();
            let myVar = setInterval(auto_show_img, 5000);
        }
        function auto_show_img() {
            show_num++;
            if (show_num == numbre_imgs) {
                show_num = 0;
            }
            $(".img_box").hide();
            $(".img_box").eq(show_num).show();

        }
        function next_img() {
            show_num++;
            if (show_num == numbre_imgs) {
                show_num = 0;
            }
            $(".img_box").hide();
            $(".img_box").eq(show_num).show();
        }
        function last_img() {
            show_num--;
            if (show_num < 0) {
                show_num = numbre_imgs - 1;
            }
            console.log(show_num);
            $(".img_box").hide();
            $(".img_box").eq(show_num).show();
        }

        return {
            start: shwo_img,
            next: next_img,
            last: last_img
        }
    }());

    show_img.start();
    $(".right_icon").click(function () {
        show_img.next();
    });
    $(".left_icon").click(function () {
        show_img.last();
    });

});