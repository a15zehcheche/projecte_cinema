$(document).ready(function () {
    $(".hide").hide();
    $(".comprar_boton").attr('disabled','disabled');
    $(".comprar_boton").css("background-color", "#aaaaaa");


    $(".email").keyup(function () {
        if (check_email($(".email").val())) {
            $(this).css("background-color", "rgb(141, 241, 128)");
            $(".comprar_boton").removeAttr('disabled')
            $(".comprar_boton").css("background-color", "rgb(41, 144, 175)");
        } else {
            $(this).css("background-color", "rgb(241, 128, 128)");
            $(".comprar_boton").attr('disabled','disabled');
            $(".comprar_boton").css("background-color", "#aaaaaa");            
        }
    });
    $(".tel").keyup(function () {
        if (phone_number($(".tel").val())) {
            $(this).css("background-color", "rgb(141, 241, 128)");
            if(check_email($(".email").val())){
                $(".comprar_boton").removeAttr('disabled')
                $(".comprar_boton").css("background-color", "rgb(41, 144, 175)");
            }
        } else {
            $(this).css("background-color", "rgb(241, 128, 128)");
            $(".comprar_boton").attr('disabled','disabled');
            $(".comprar_boton").css("background-color", "#aaaaaa");
        }
    });
    
});

function check_email(email) {
    let emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    return emailRegex.test(email);
}
function phone_number(tel) {
    let telRegex = /^(9|6|7)\d{8}$/;
    return telRegex.test(tel);
}
