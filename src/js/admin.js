$(document).ready(function () {

    //Funcions---------------------------------------------------------------------------------------

    function DibujarTabla(vip,butacas_ocupats){
        let butaques = `<table>`; 
        for(let i=12;i>0;i--){
            butaques += `<tr>`;
            for(let j=0;j<=9;j++){
                let ocupat =0;
                for(let l = 0; l<butacas_ocupats.length;l++){
                    if(butacas_ocupats[l]==`${i}${j}`){
                        ocupat=1;
                    }
                }
                if(i==vip){
                    if(ocupat)
                    butaques += `<td num="${i}${j}" class="ocupat"><img src='../img/ocupat.png'></td>`;
                    else
                    butaques += `<td num="${i}${j}"><img src='../img/lliurevip.png'></td>`;
                }
                else{
                    if(ocupat)
                    butaques += `<td num="${i}${j}" class="ocupat"><img src='../img/ocupat.png'></td>`;
                    else
                    butaques += `<td num="${i}${j}"><img src='../img/disponible.png'></td>`;
                }
               
            }
            butaques += `</tr>`;
        }
        butaques += `<tr><th colspan="13" class="pantalla"><img src="../img/pantalla_abajo.png"></img></th></tr>`;
        butaques += `</table>`;
        //butaques += `<div  class="pantalla"><img src="img/pantalla_abajo.png"></img><div>`;
        return butaques;
    }


    //Agafar Dades que venen de PHP-------------------------------------------------------------------
    let preu = parseInt($(".preu").text());
    let filaVip = parseInt($(".fila").text());
    $(".no_display").hide();
    let str_butaques = $.trim($(".butacas_ocupat").text());
    let butacas_ocupats = str_butaques.split(" ");
    let data = $(".date").attr("value");
    let dia_semana = new Date(data).getDay();
    if(dia_semana == 3){
        $(".espectador").html("<p><span class='letter_dia'>Dia </span><span class='letter_espec'>Espectectador!</span></p>");
    }
    $(".mostra_butaques").html(DibujarTabla(filaVip,butacas_ocupats));
    let VipsVenuts = 0;
    let NormalsVenuts = 0;
    for(let i =0; i<butacas_ocupats.length;i++){
        if(butacas_ocupats[i]!=""){
            if(butacas_ocupats[i].startsWith(filaVip)){
                VipsVenuts++;
            }
            else{
                NormalsVenuts++;
            }
        }
    }
    console.log(butacas_ocupats);
    butacas_totals = 120;
    butacas_vips= 10;
    NormalsNoVenuts = (butacas_totals-butacas_vips) - NormalsVenuts;
    VipsNovenuts = butacas_vips - VipsVenuts;
    butacas_vendidos= VipsVenuts+NormalsVenuts;
    butcas_novendidos = butacas_totals - butacas_vendidos;
    let recaudacionormal = preu*NormalsVenuts;
    let recaudaciovip = (preu+2)*VipsVenuts;
    let recaudaciototal = recaudacionormal + recaudaciovip;

    let grafic1 = document.getElementById("grafic1");
    let grafic2 = document.getElementById("grafic2");
    let grafic3 = document.getElementById("grafic3");

    let Grafic1 = new Chart(grafic1,{
        type: 'pie',
        data:{
            labels: ['Butacas venudes','Butacas no venudes'],
            datasets:[
                {
                    label: 'Points',
                    backgroundColor:['#FD9B37','#C6CBD2'],
                    data:[butacas_vendidos,butcas_novendidos]
                }
            ]
        },
        options:{
            animation:{
                animateScale:true
            }
        }
    })

    let Grafic2 = new Chart(grafic2,{
        type: 'pie',
        data:{
            labels: ['Normals venudes','Normals no Venudes'],
            datasets:[
                {
                    label: 'Points',
                    backgroundColor:['#FD9B37','#C6CBD2'],
                    data:[NormalsVenuts,NormalsNoVenuts]
                }
            ]
        },
        options:{
            animation:{
                animateScale:true
            }
        }
    })

    let Grafic3 = new Chart(grafic3,{
        type: 'pie',
        data:{
            labels: ['Vips venudes','Vips no venudes'],
            datasets:[
                {
                    label: 'Points',
                    backgroundColor:['#FD9B37','#63639C'],
                    data:[VipsVenuts,VipsNovenuts]
                }
            ]
        },
        options:{
            animation:{
                animateScale:true
            }
        }
    })

    $(".Mostrar_preu").html(`<p>Recaudació butaques normals:<b>${recaudacionormal}€</b></p><p>Recaudació butaques vips:<b>${recaudaciovip}€</b></p><p>Recaudació total:<b>${recaudaciototal}€</b></p>`);
})