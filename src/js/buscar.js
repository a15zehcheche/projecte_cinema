var toDay = new Date();
function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}


function diaSetmanaString(num) {
    if (num == 1)
        return "Dl.";
    else if (num == 2)
        return "Dm.";
    else if (num == 3)
        return "Dc.";
    else if (num == 4)
        return "Dj.";
    else if (num == 5)
        return "Dv.";
    else if (num == 6)
        return "Ds.";
    else
        return "Dg.";
}

$(document).ready(function () {
    //calendari setmana
    let dia = toDay.getDate();
    let mes = toDay.getMonth()+1;
    let any = toDay.getFullYear();

    function dibujarSetmana(dia,mes,any){ 
        let dia_max_mes = daysInMonth(mes, any);
        console.log("in date-----dia: "+dia+" mes: "+mes+" any: "+any);
        let copy_dia = dia;
        let copy_mes = mes;
        let copy_any = any;

        let codi_html = `<table><tr>`;
    
        codi_html += "<tr>";
        for(i = 0; i < 7; i++){
            if (dia > dia_max_mes) {
                dia = 1;
                mes++;
                if(mes>12){
                    mes = 1
                    any++;
                }
                dia_max_mes = daysInMonth(mes, any);
            }
            new_data = new Date(`${mes}/${dia}/${any}`);
            //console.log(new_data);
            num_dia = new_data.getDay();
            //console.log(num_dia);
            codi_html+=`<th>${diaSetmanaString(num_dia)}</th>`;
            dia++;
        }
        codi_html += "</tr>";

        dia = copy_dia;
        mes = copy_mes;
        any = copy_any;
        dia_max_mes = daysInMonth(mes, any);
     
        for (i = 0; i < 7; i++) {
            if (dia > dia_max_mes) {
                dia = 1;
                mes++;
                if(mes>12){
                    mes = 1
                    any++;
                }
                dia_max_mes = daysInMonth(mes, any);
            }
            codi_html += ` <td><div><a href='buscar.php?data=${any}-${mes}-${dia}'>${dia} - ${mes} - ${any}</a></div></td>`;
            //console.log(dia);
            dia++;
        }
        codi_html += '</tr></table>';
        //console.log(codi_html);
        return codi_html;
    }
    $(".lastWeek").hide();
    $(".lastWeek").click(function(){
        dia -=7;
        let dia_max_mes = daysInMonth(mes, any);
        if(dia<=0){
            if(--mes < 1){
                mes = 12;
                any--;
                dia_max_mes = daysInMonth(mes, any);
            }

            dia = dia_max_mes + (dia);
            console.log("dia: "+ dia);

        }
        $(".calendari_setmana").html(dibujarSetmana(dia,mes,any));

        if(dia == toDay.getDate() && mes == toDay.getMonth()+1 && any == toDay.getFullYear()){
            $(".lastWeek").hide();
        }else{
            $(".lastWeek").show();
        }
    });
    $(".nextWeek").click(function(){
        
        dia +=7;
        let dia_max_mes = daysInMonth(mes, any);
        if(dia>dia_max_mes){
           dia = dia - dia_max_mes;
            if(++mes > 12){
                mes=1;
                any++;
            }
        }

        $(".calendari_setmana").html(dibujarSetmana(dia,mes,any));
        $(".lastWeek").show();
    });
    
    $(".calendari_setmana").html(dibujarSetmana(dia,mes,any));
});