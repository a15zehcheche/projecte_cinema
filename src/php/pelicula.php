<!DOCTYPE html>
<html lang="ES">
<head>
    <meta charset="UTF-8">
    <script src="../js/jquery.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/skeleton.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/pelicula.css">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:200,200i,300,300i,400,400i,600,600i,700,700i,900"
        rel="stylesheet">
    <title>
        <?php echo $_GET["nomPeli"] ?>
    </title>
</head>
<body>
    <!--------------------------------------------HEADER----------------------------------------->
    <header>
        <div class="conten">
            <div class="logo">
                <img src="../img/IAM_CAT_logos.png" alt="imatge">
            </div>
            <div class="menu">
                <a href="../index.php">Home</a>
                <a href="consulta.php">Consulta</a>
                <a href="buscar.php">Buscar</a>
            </div>
        </div>
    </header>
    <!--------------------------------------CONTENT---------------------------------------------------->
    <div class="marginCos">
        <h1 class="titol">
            <?php echo "<h3>";
                    $nompeli=$_GET["nomPeli"];
                    echo  $nompeli." - ";
                    if (isset($_GET["fecha"])) {
                        $newDate= date("l", strtotime($_GET["fecha"]));
                        if ($newDate == "Monday") {$newDate = " DILLUNS ";} 
                        else if ($newDate == "Tuesday") {$newDate = " <u>DIMARTS</u> ";} 
                        else if ($newDate == "Wednesday") {$newDate = " <u>DIMERCRES</u> ";} 
                        else if ($newDate == "Thursday") {$newDate = " <u>DIJOUS</u> ";} 
                        else if ($newDate == "Friday") {$newDate = " <u>DIVENDRES</u> ";} 
                        else if ($newDate == "Saturday") {$newDate = " <u>DISSABTE</u> ";} 
                        else if ($newDate == "Sunday") {$newDate = " <u>DIUMENGE</u> ";}
                        echo $newDate;
                        echo "a les"." ".substr($_GET["hora"],0,-3);
    
                        echo "</h3>"; 
                    }
            ?>
        </h1>
        <div class="lineaSeparador"></div>
        <?php
            /**************DB CONNECT**************/
            require_once 'login.php';
            $db_server = mysqli_connect($db_hostname, $db_username, $db_password, $db_database);
            $db_server->set_charset("utf8");
            if (!$db_server) {
                die("Unable to connect to MySQL: " . mysql_error());
            }//imprimeix a msg i acaba l'script

            $nompeliculita = $_GET["nomPeli"];
            $sql = "SELECT imgUrl from pelicula WHERE nomPeli='$nompeliculita'";
            $result = mysqli_query($db_server, $sql);
            $fecha = $_GET["fecha"];
            if (!$result) {
                die("Database access failed: " . mysql_error());
            }
            /*************CON FECHA**************************************************** */
            if (isset($_GET["fecha"])) {
                echo "<div class='horario'>";
                echo "<a class='button reserva' href='butaques.php?nomPeli=$nompeliculita&fecha=$fecha'>Reserva Entrades</a>";
                $rows = mysqli_num_rows($result);
                for ($i = 0; $i < $rows; $i++) {
                    $consulta = mysqli_fetch_assoc($result);
                    foreach ($consulta as $key => $valor) {
                        echo "<div class='poster'><img class='movie_poster' src=../$valor></a></div>";
                        echo "<br>";
                    }
                }
                echo "</div>";
            }
            /*************SIN FECHA**************************************************** generamos todas las fechas > HOY*/
            else {
                $fechita = "SELECT fecha,hora from seccio where nomPeli = '$nompeliculita' and fecha>=CURDATE()";
                $result_fechas = mysqli_query($db_server, $fechita);
                $rows = mysqli_num_rows($result_fechas);
               
                if($rows ==0){
                    echo ("<div class='warningBox'><h3> No es va trobar cap sessió per aquesta pelicula.</h3></div>" );
                }
                echo "<div class='horario'>";
                for ($i = 0; $i < $rows; $i++) {
                    $consulta = mysqli_fetch_assoc($result_fechas);
                    foreach ($consulta as $key => $valor){
                        if($key == "fecha") {////FECHAS
                            $originalDate = $valor;
                            $newDate = date("l", strtotime($originalDate));
                            if ($newDate == "Monday") {$dia = " DILLUNS ";} else if ($newDate == "Tuesday") {$dia = " <u>DIMARTS</u> ";} else if ($newDate == "Wednesday") {$dia = " <u>DIMERCRES</u> ";} else if ($newDate == "Thursday") {$dia = " <u>DIJOUS</u> ";} else if ($newDate == "Friday") {$dia = " <u>DIVENDRES</u> ";} else if ($newDate == "Saturday") {$dia = " <u>DISSABTE</u> ";} else if ($newDate == "Sunday") {$dia = " <u>DIUMENGE</u> ";}
                            echo "<a class='button reserva' href='butaques.php?nomPeli=$nompeliculita&fecha=$valor'><u>$valor</u> el $dia";
                        }else{
                            $valor = substr($valor, 0, -3);
                            echo "  A LES   <u>$valor</u></a>";
                        }
                    }
                }
                
                //div horario ////// IMAGEN
                $rows = mysqli_num_rows($result);
                for ($i = 0; $i < $rows; $i++) {
                    $consulta = mysqli_fetch_assoc($result);
                    foreach ($consulta as $key => $valor) {
                        if ($key == "imgUrl") {
                            echo "<div class='poster'><img class='movie_poster' src=../$valor></div>";

                        }
                        echo "<br>";
                    }
                }
            }
            echo "</div>";
            mysqli_close($db_server);
        ?>
    </div>
    <!------------------------FOOOOOOOOOOOOOOOtER---------------------------------------------------->

    <footer>
        <div><span>Copyright © 2018</span></div>
    </footer>
</body>

</html> 