<!DOCTYPE html>
<html lang="ES">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="../js/jquery.js"></script>
    <script src="../js/butaques.js"></script>
    <script src="../js/sweetalert2.all.min.js"></script>
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/skeleton.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/butaques.css">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:200,200i,300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">
    

    <title>CINEMA</title>
</head>

<body>
    <header>
        <div class="conten">
            <div class="logo">
                <img src="../img/IAM_CAT_logos.png" alt="imatge">
            </div>
            <div class="menu">
                <a href="../index.php">Home</a>
                <a href="./consulta.php">Consulta</a>
                <a href="./buscar.php">Buscar</a>
            </div>
        </div>
    </header>
    
    <div class="marginCos">
        <h1 class="titol">Selecciona butaques del dia</h1>
        <div class="lineaSeparador"></div>
        <?php
            $nomPeli= $_GET['nomPeli'];
            $date= $_GET['fecha'];
            if($nomPeli == null || $date == null){
                echo "<p class='no_display error'>1</p>";
                echo "<div class='errorBox'><h1>ERROR</h1></div>";
            }else{
                echo "<p class='no_display error'>0</p>";
            }
        ?>
        <div class="content">
            <div class="espectador">
            </div>
            <div class="llegenda">
            <p><span><img src="../img/disponible.png">Disponibles</span><span><img src="../img/lliurevip.png">Disponibles Vips</span><span><img src="../img/escollida.png">Escollides</span><span><img src="../img/ocupat.png">Ocupades</span></p>
            </div>
            <form action="form.php" method="POST">
            <div class="row contenido">
                <div class="eight columns sala">
                </div>
                <div class="four columns info_compra">
                    <h3>Resum</h3>
                    <?php
                        //INICI CONNECXIO DB
                        require "login.php";
                        $db_server = mysqli_connect($db_hostname,$db_username, $db_password,$db_database);
                        if (!$db_server) {
                            die("Connection failed: " . mysqli_connect_error());
                        }
                        $db_server->set_charset("utf8");
                        $sql = "USE $db_database";
                        mysqli_query($db_server, $sql);

                        //INICI GETDATA
                        echo "<input type='date' class='no_display date' name='data' value='$date'>";
                        //INICI GETPREU
                        $sql = "SELECT preu_base FROM pelicula WHERE nomPeli='$nomPeli'";
                        $result = mysqli_query($db_server, $sql);
                        $consulta = mysqli_fetch_assoc($result);
                        $preu = $consulta['preu_base'];
                        if(date("w",strtotime($date))==3){
                            $preu-=2;
                            echo "<p class='no_display preu'>$preu</p>";
                        }
                        else{
                            echo "<p class='no_display preu'>$preu</p>";
                        }
                        
                        //INICI GETFILA
                        $sql = "SELECT idSala FROM seccio WHERE fecha='$date'";
                        $result = mysqli_query($db_server, $sql);
                        $consulta = mysqli_fetch_assoc($result);
                        $idSala = $consulta['idSala'];
                        $sql = "SELECT filaVip FROM sala WHERE idSala='$idSala'";
                        $result = mysqli_query($db_server, $sql);
                        $consulta = mysqli_fetch_assoc($result);
                        $filaVip = $consulta['filaVip'];
                        echo "<p class='no_display fila'>$filaVip</p>";
                        echo "<input type='text' class='no_display' name='filaVip' value='$filaVip' >";

                        //INICI GETBUTAQUES_OCUPATs
                        $butcas_ocupat = array();
                        $sql = "SELECT codi_entrada FROM entrada WHERE fecha='$date'";
                        $result = mysqli_query($db_server, $sql);
                        $rows = mysqli_num_rows($result);
                        for ($i = 0 ; $i < $rows ; $i++)
                        {
                            $consulta = mysqli_fetch_assoc($result);
                            foreach ($consulta as $key => $valor){
                                $sql2 = "SELECT num_butaques FROM butaquesSeleccionat WHERE codi_entrada=$valor";
                                $result2 = mysqli_query($db_server, $sql2);
                                $rows2 = mysqli_num_rows($result2);
                                for ($j = 0 ; $j < $rows2 ; $j++)
                                {
                                    $consulta2 = mysqli_fetch_assoc($result2);
                                    foreach ($consulta2 as $key2 => $valor2){
                                        array_push($butcas_ocupat,$valor2);
                                    }
                                    
                                }
                            }
                        }
                        $count = count($butcas_ocupat);
                        echo "<p class='no_display butacas_ocupat'>";
                        foreach ($butcas_ocupat as $key => $valor){
                            echo $valor." ";
                        }
                        echo "</p>";
                        mysqli_close ($db_server);                      
                    ?>
                    <input type="text" class="preu_total no_display" name="preu_total" value="">
                    <p><span class="quant_vips"></span> X vips <span class="euro_vips">0</span>€</p>
                    <p><span class="quant_normal"></span> X normals <span class="euro_normal">0</span>€</p>
                    <p>Butaques Seleccionades:</p>
                    <input type="text" class="seleccionats no_display" name="seleccionats" value="">
                    <div class="display_butaques_select"></div>
                    <p>Preu total: <span class="preu_total">0</span>€</p>
                </div>
            </div>
            <div class="row contenido">
                    <div class="nine columns">
                        <br>
                    </div>
                    <div class="three columns">
                        <input class="submit" type="submit" value="Comprar">
                    </div>
            </div>
            </form>
        </div>
    </div>
    <footer>
        <div><span>Copyright © 2018</span></div>
    </footer>

</body>

</html>