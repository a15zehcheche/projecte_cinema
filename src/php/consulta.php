<!DOCTYPE html>
<html lang="ES">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="../js/jquery.js"></script>
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/skeleton.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/consulta.css">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:200,200i,300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">


    <title>CINEMA</title>
</head>

<body>
    <header>
        <div class="conten">
            <div class="logo">
                <img src="../img/IAM_CAT_logos.png" alt="imatge">
            </div>
            <div class="menu">
                <a href="../index.php">Home</a>
                <a href="consulta.php" class="crumb">Consulta</a>
                <a href="buscar.php">Buscar</a>
            </div>
        </div>
    </header>

    <div class="marginCos">
        <h1 class="titol">Consulta reserva futura</h1>
        <div class="lineaSeparador"></div>
        <br>
        <form method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?> ">
            <span>El teu email: </span>
            <input type="text" name="email" placeholder="Email">
            <input type="submit" value="buscar">
        </form>
<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST' || $_SERVER['REQUEST_METHOD'] == 'GET') {
    require_once 'login.php';
    $db_server = mysqli_connect($db_hostname, $db_username, $db_password, $db_database);
    if (!$db_server) {
        die("Connection failed: " . mysqli_connect_error());
    }
    //echo "Connected successfully<br>";
    $db_server->set_charset("utf8");

    $email = $_POST["email"];
    if ($_SERVER['REQUEST_METHOD'] == 'GET') {
        $email = $_GET["email"];
    }
    //comprova si te una reserva futura
    $sql = "SELECT * from $db_database.entrada where email= '$email'";
    $result = mysqli_query($db_server, $sql);
    $rows = mysqli_num_rows($result);
    for ($i = 0; $i < $rows; $i++) {
        $consulta = mysqli_fetch_assoc($result);
        foreach ($consulta as $key => $valor) {

            if ($key == "fecha" && $valor >= date("Y-m-d")) {
                $fecha = $valor;
            }
            if ($key == "codi_entrada") {
                $codi_entrada = $valor;
            }
        }
    }

    //---------------------------------
    if ($fecha == null) {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            echo '<div class="info_no_entrada row"><h1 >No tens niguna reserva</h1></div>';
        }

    } else {
        $sql = "SELECT * from $db_database.entrada where codi_entrada = '$codi_entrada'";
        $result = mysqli_query($db_server, $sql);

        for ($i = 0; $i < $rows; $i++) {
            $consulta = mysqli_fetch_assoc($result);
            foreach ($consulta as $key => $valor) {
                if ($key == "codi_entrada") {
                    $codiEntrada = $valor;
                } elseif ($key == "fetcha") {
                    $fecha = $valor;
                } elseif ($key == "preu_total") {
                    $preu_total = $valor;
                } elseif ($key == "idSeccio") {
                    $idSeccio = $valor;
                }
            }
        }
        //echo $codiEntrada . "<br>";
        //echo $fecha . "<br>";
        //echo $preu_total . "<br>";
        //echo $idSeccio . "<br>";
        //aconsegir el hora, idSala i el nom de la pelicula
        $sql = "SELECT * from $db_database.seccio where idSeccio= '$idSeccio'";
        $result = mysqli_query($db_server, $sql);
        $rows = mysqli_num_rows($result);
        for ($i = 0; $i < $rows; $i++) {
            $consulta = mysqli_fetch_assoc($result);
            foreach ($consulta as $key => $valor) {
                if ($key == "hora") {
                    $hora = $valor;
                } elseif ($key == "idSala") {
                    $idSala = $valor;
                } elseif ($key == "nomPeli") {
                    $nomPeli = $valor;
                }
            }
        }

        //echo $hora . "<br>";
        //echo $idSala . "<br>";
        //echo $nomPeli . "<br>";
        //agonsegir el url de la imatge
        $sql = "SELECT imgUrl from $db_database.pelicula where nomPeli= '$nomPeli'";
        $result = mysqli_query($db_server, $sql);
        $consulta = mysqli_fetch_assoc($result);
        $imgUrl = $consulta["imgUrl"];

        //echo $imgUrl . "<br>";

        //consultas de les butaques.
        $butcas_seleccionat = array();
        $sql2 = "SELECT num_butaques FROM butaquesSeleccionat WHERE codi_entrada=$codiEntrada";
        $result2 = mysqli_query($db_server, $sql2);
        $rows2 = mysqli_num_rows($result2);
        for ($j = 0; $j < $rows2; $j++) {
            $consulta2 = mysqli_fetch_assoc($result2);
            foreach ($consulta2 as $key2 => $valor2) {
                array_push($butcas_seleccionat, $valor2);
            }

        }
        $numButaques = count($butcas_seleccionat);
        $butcas_seleccionat_string = "";
        for ($i = 0; $i < $numButaques; $i++) {
            $butcas_seleccionat_string .= $butcas_seleccionat[$i] . "&nbsp;&nbsp;";
        }

        //echo $numButaques . "<br>";
        //echo $butcas_seleccionat_string . "<br>";

        //fila vip
        $sql = "SELECT filaVip FROM sala WHERE idSala='$idSala'";
        $result = mysqli_query($db_server, $sql);
        $consulta = mysqli_fetch_assoc($result);
        $filaVip = $consulta['filaVip'];
        //echo $filaVip . "<br>";

        //saber quantitat de vips i normals
        $quan_vips = 0;
        $quan_normal = 0;
        for ($i = 0; $i < $numButaques; $i++) {
            if (substr($butcas_seleccionat[$i], 0, 1) == $filaVip) {
                $quan_vips++;
            } else {
                $quan_normal++;
            }
        }
        //echo $quan_vips . "<br>";
        //echo $quan_normal . "<br>";

        //general el codi html
        echo "<h3 class='codiEntrada'>El teu codi d'entrada es $codiEntrada</h3>
        <div class='row'>
           <div class='three columns'>
               <div class='info_entrada_img'><img src='../$imgUrl' width='100%'></div>
           </div>
           <div class='seven columns'>
               <div class='info_entrada'>";
        echo "<h3>$nomPeli</h3><p>$fecha $hora</p><p>Sala: $idSala</p><p>Numero butaques: $numButaques</p>
        <p>Butaques reservades: $butcas_seleccionat_string</p><div class='info_no_entrada_hr'></div>
        <p>Vips x $quan_vips</p><p>Normals x $quan_normal</p><h4 class='preu_total'>Preu total $preu_total €</h4></div>
        </div></div>";

    }

    mysqli_close($db_server);
}

?>
    </div>
    <footer>
        <div><span>Copyright © 2018</span></div>
    </footer>

</body>

</html>
