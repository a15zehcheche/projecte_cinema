<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="../js/jquery.js"></script>
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/skeleton.css">
    <link rel="stylesheet" href="../css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:200,200i,300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">
    <title>CINEMA</title>
</head>

<body>
    <header>
        <div class="conten">
            <div class="logo">
                <img src="../img/IAM_CAT_logos.png" alt="imatge">
            </div>
            <div class="menu">
                <a href="../index.php">Home</a>
                <a href="consulta.php">Consulta</a>
                <a href="buscar.php">Buscar</a>
            </div>
        </div>
    </header>
    <div class="marginCos">
        <h1 class="titol">Pagament</h1>
    <div class="lineaSeparador"></div>

<?php
require_once 'login.php';
$db_server = mysqli_connect($db_hostname, $db_username, $db_password, $db_database);
if (!$db_server) {
    die("Connection failed: " . mysqli_connect_error());
}
//echo "Connected successfully<br>";
$db_server->set_charset("utf8");
// evitar atacs xss
function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
//valots rebuts

$nom = test_input($_POST["nom"]);
$cognom = test_input($_POST["cognom"]);
$tel = test_input($_POST["tel"]);
$email = test_input($_POST["email"]);
$filaVip = test_input($_POST["filaVip"]);
$seleccionats = test_input($_POST["seleccionats"]);
//pasar un string a un array
$arraySeleccionats = explode(",", $seleccionats);
$data = test_input($_POST["data"]);
$preu_total = test_input($_POST["preu_total"]);

if ($email != null && $seleccionats !=null)  {
    //validacion de email i telefon
    $valit = true;
    $tel_length = mb_strlen($tel);
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        if($tel!=null){
            if ($tel_length == 9) {
                $valit = true;
            } else {
                $valit = false;
                echo ("<div class='warningBox'><h1>Telèfon no vàlid.</h1></div>");
            }
        }
      
    } else {
        $valit = false;
        echo ("<div class='warningBox'><h1>Email no vàlid.</h1></div>");
    }

    //validacion numbre de butaques.
    if (count($arraySeleccionats) > 10) {
        $valit = false;
        echo ("<div class='warningBox'><h1>Error en les butaques seleccionades</h1></div>");
    }

    //busca la id de la sseccio
    $sql = "SELECT idSeccio from $db_database.seccio where fecha = '$data'";
    $result = mysqli_query($db_server, $sql);
    $consulta = mysqli_fetch_assoc($result);
    $idSeccio = $consulta['idSeccio'];

    $query = "SELECT nomPeli FROM seccio WHERE idSeccio = '$idSeccio'"; //creació de la query
    $result = mysqli_query($db_server, $query);
    if (!$result) {
        die("Database access failed: " . mysql_error());
    }
    //mostra el resultat.
    $consulta = mysqli_fetch_assoc($result);
    $nomPeli = $consulta["nomPeli"];

    if ($valit) {
        //comprovar si el usuari esisteix
        $sql = "SELECT COUNT(email) from $db_database.usuari where email = '$email'";
        $result = mysqli_query($db_server, $sql);
        $consulta = mysqli_fetch_assoc($result);
//el usuari no esisteix
        if ($consulta['COUNT(email)'] == 0) {
            //insert el usuari
            if($nom != null && $cognom != null && $tel != null )
                $sql = "INSERT INTO $db_database.usuari VALUE ('$email','$nom','$cognom','$tel')";
            else{
                $sql = "INSERT INTO $db_database.usuari (email) VALUE ('$email')";
            }
            mysqli_query($db_server, $sql);
        }

//comprova si te una reserva futura
        $sql = "SELECT * from $db_database.entrada where email= '$email'";
        $result = mysqli_query($db_server, $sql);
        $rows = mysqli_num_rows($result);
        for ($i = 0; $i < $rows; $i++) {
            $consulta = mysqli_fetch_assoc($result);
            foreach ($consulta as $key => $valor) {
                if ($key == "fecha" && $valor >= date("Y-m-d")) {
                    $fecha = $valor;
                }
            }
        }
//si no te una entrada futura
        if ($fecha == null) {
            //comprova tots els butaques que vol esta lliure.
            $butcas_ocupat = array();
            $sql = "SELECT codi_entrada FROM entrada WHERE fecha='$data'";
            $result = mysqli_query($db_server, $sql);
            $rows = mysqli_num_rows($result);
            for ($i = 0; $i < $rows; $i++) {
                $consulta = mysqli_fetch_assoc($result);
                foreach ($consulta as $key => $valor) {
                    $sql2 = "SELECT num_butaques FROM butaquesSeleccionat WHERE codi_entrada=$valor";
                    $result2 = mysqli_query($db_server, $sql2);
                    $rows2 = mysqli_num_rows($result2);
                    for ($j = 0; $j < $rows2; $j++) {
                        $consulta2 = mysqli_fetch_assoc($result2);
                        foreach ($consulta2 as $key2 => $valor2) {
                            array_push($butcas_ocupat, $valor2);
                        }

                    }
                }
            }
            $butcas_libre = true;
            for ($i = 0; $i < count($arraySeleccionats) && $butcas_libre; $i++) {
                for ($j = 0; $j < count($butcas_ocupat) && $butcas_libre; $j++) {
                    if ($arraySeleccionats[$i] == $butcas_ocupat[$j]) {
                        $butcas_libre = false;
                    }
                }

            }
            if ($butcas_libre) {
                //general el codi de entrada
                $sql = "SELECT codi_entrada from $db_database.entrada where fecha = '$data'";
                $codiEntradaExist = array();

                $result = mysqli_query($db_server, $sql);
                $rows = mysqli_num_rows($result);
                for ($i = 0; $i < $rows; $i++) {
                    $consulta = mysqli_fetch_assoc($result);
                    foreach ($consulta as $key => $valor) {
                        array_push($codiEntradaExist, $valor);
                    }
                }
                $esUnic = 1;
                do {
                    $codiEntrada = rand(100000, 999999);
                    for ($i = 0; $i < $codiEntradaExist . length; $i++) {
                        if ($codiEntradaExist[$i] == $codiEntrada) {
                            $esUnic = 0;
                        }
                    }
                } while (!$esUnic);
                //print($codiEntrada);

                //insert el entrada
                $sql = "INSERT INTO $db_database.entrada VALUES ($codiEntrada,'$email',$idSeccio,'$data',$preu_total)";
                mysqli_query($db_server, $sql);

                //insert els butaques
                for ($i = 0; $i < count($arraySeleccionats); $i++) {
                    $sql = "INSERT INTO $db_database.butaquesSeleccionat VALUES ($codiEntrada,$arraySeleccionats[$i])";
                    mysqli_query($db_server, $sql);
                }
                echo "<div class='infoBox'><h1>Reservat. El codi de entrada és $codiEntrada </h1></div>";
                echo "<a href='consulta.php?email=$email'> Clica per més informació</a>";

            } else {
                echo "<div class='warningBox'><h1>Una de les butaques seleccionades, està reservada per un altre client </h1></div>";
                echo "<a href='butaques.php?fecha=$data&nomPeli=$nomPeli'>Torna a reservar butaques</a>";
            }

        } else {
            $sql = "SELECT codi_entrada from $db_database.entrada where email= '$email'";
            $result = mysqli_query($db_server, $sql);
            $consulta = mysqli_fetch_assoc($result);
            $codiEntrada = $consulta['codi_entrada'];
            echo "<div class='warningBox'><h1>Ja tens una reserva, el teu codi de entrada es $codiEntrada </h1></div>";
            echo "<a href='consulta.php?email=$email'> Clica per consultar-la</a>";
        }
    }
} else {
    echo "<div class='errorBox'><h1>Error</h1></div>";
    echo "<a href='../index.php'> Torna a la pàgina principal</a>";
}

/*
print_r($arraySeleccionats). "<br>";
echo "numbre de butaques: ".count($arraySeleccionats). "<br>";
echo "idSeccio".$idSeccio. "<br>";
echo "preTotal" .$preu_total. "<br>";
echo "email" . $_POST["email"] . "<br>";
echo "filaVip" . $_POST["filaVip"] . "<br>";
echo "seleccionats" . $_POST["seleccionats"] . "<br>";
echo "data" . $_POST["data"] . "<br>";
*/

?>
    </div>

<footer>
    <div><span>Copyright © 2018</span></div>
</footer>

</body>

</html>
