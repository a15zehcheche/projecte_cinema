<!DOCTYPE html>
<html lang="ES">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="../js/jquery.js"></script>
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/skeleton.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/buscar.css">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:200,200i,300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">
    <script src="../js/buscar.js"></script>

    <title>CINEMA</title>
</head>

<body>
    <header>
        <div class="conten">
            <div class="logo">
                <img src="../img/IAM_CAT_logos.png" alt="imatge">
            </div>
            <div class="menu">
                <a href="../index.php">Home</a>
                <a href="./consulta.php">Consulta</a>
                <a href="./buscar.php" class="crumb">Buscar</a>
            </div>
        </div>
    </header>

    <div class="marginCos">
        <h1 class="titol">Buscar pel·lícula</h1>
        <div class="lineaSeparador"></div>
        <br>
        <div class="calendari_setmana"></div>
        <button class="lastWeek" type="button">Anterior</button>
        <button class="nextWeek" type="button">Seguent</button>
        <br>
        <br>
        <form method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?> ">
            <input type="text" name="buscar" placeholder="Buscar pelicula per nom">
            <input type="submit" value="buscar">
            <div class="info">*Clica per mostrar totes les pel·lícules o per nom.</div>
        </form>
        



        <div class="pelicula">
        <?php
if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    require_once 'login.php';
    $db_server = mysqli_connect($db_hostname, $db_username, $db_password, $db_database);
    $db_server->set_charset("utf8");
    $date = $_GET["data"];
    $data = date("Y-m-d", strtotime($date));
    if ($data != null) {
        $query = "SELECT COUNT(*) FROM seccio WHERE fecha = '$data'"; //creació de la query
        $result = mysqli_query($db_server, $query);
        if (!$result) {
            die("Database access failed: " . mysql_error());
        }
        //mostra el resultat.
        $consulta = mysqli_fetch_assoc($result);
        if ($consulta["COUNT(*)"] > 0) {
            $query = "SELECT hora FROM seccio WHERE fecha = '$data'"; //creació de la query
            $result = mysqli_query($db_server, $query);
            if (!$result) {
                die("Database access failed: " . mysql_error());
            }
            //mostra el resultat.
            $consulta = mysqli_fetch_assoc($result);
            $PeliHora = $consulta["hora"];

            //selecciona el seccio de la data indicada.
            $query = "SELECT nomPeli FROM seccio WHERE fecha = '$data'"; //creació de la query
            $result = mysqli_query($db_server, $query);
            if (!$result) {
                die("Database access failed: " . mysql_error());
            }

            $rows = mysqli_num_rows($result);

            //si hi ha mes de un peli per dia
            $codi_html = "";
            for ($i = 0; $i < $rows; $i++) {
                //mostra el resultat.
                $consulta = mysqli_fetch_assoc($result);
                foreach ($consulta as $key => $valor) {
                    $nomPeli = $valor;
                    $query = "SELECT * FROM pelicula WHERE nomPeli = '$nomPeli'"; //creació de la query

                    $result2 = mysqli_query($db_server, $query);
                    if (!$result2) {
                        die("Database access failed: " . mysql_error());
                    }
                    //mostra el resultat.

                    $rows2 = mysqli_num_rows($result2);

                    for ($j = 0; $j < $rows2; $j++) {
                        $codi_html .= '<div class="item">';

                        $consulta2 = mysqli_fetch_assoc($result2);
                        foreach ($consulta2 as $key => $valor) {
                            if ($key == "nomPeli") {
                                $nomPeli = $valor;
                                $div_titol = "<div class='text_conten'>
                    <a href='pelicula.php?nomPeli=$nomPeli&fecha=$data&hora=$PeliHora'>
                    <h3>$valor</h3>
                    </a>
                    </div>";
                            } else if ($key == "imgUrl") {
                                $div_img = "<div class='img_conten'>
                    <a href='pelicula.php?nomPeli=$nomPeli&fecha=$data&hora=$PeliHora'>
                    <img class='key_img' src='../$valor' alt='photo'>
                    </a>
                    </div>";
                            }
                        }

                        $codi_html .= $div_img;
                        $codi_html .= $div_titol;
                        $codi_html .= "<div class='text_data_conten'><h3>" . $data . "</h3> </div>";
                        $codi_html .= '</div>';
                    }

                }
            }
            mysqli_close($db_server);
        } else {
            $codi_html = "<h3 class='resultats'>No, hi ha pelis el dia seleccionat!</h3>";
        }
        echo $codi_html;
    }
}
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    require_once 'login.php';
    $db_server = mysqli_connect($db_hostname, $db_username, $db_password, $db_database);
    $db_server->set_charset("utf8");

    //print_r($db_server);
    if (!$db_server) {
        die("Unable to connect to MySQL: " . mysql_error());
    }
    //imprimeeix a msg i acaba l'script

    $nom = $_POST['buscar'];
    $sql = "SELECT * from pelicula WHERE nomPeli LIKE '%$nom%'";
    $result = mysqli_query($db_server, $sql);

    if (!$result) {
        die("Database access failed: " . mysql_error());
    }

    $rows = mysqli_num_rows($result);
    for ($i = 0; $i < $rows; $i++) {
        $consulta = mysqli_fetch_assoc($result);
        foreach ($consulta as $key => $valor) {
            if ($key == "nomPeli") {
                $nomPeli = $valor;
                $div_titol = "<div class='text_conten'><a href='pelicula.php?nomPeli=$nomPeli'><h3>$valor</h3></a></div>";
            } else if ($key == "imgUrl") {
                $div_img = "<div class='img_conten'><a href='pelicula.php?nomPeli=$nomPeli'><img class='key_img' src='../$valor' alt='photo'></a></div>";
            }
        }
        echo " <div class='item'>";
        echo $div_img;
        echo $div_titol;
        echo "</div>";
    }
    if ($rows == 0) {
        echo "<h3 class='resultats'>No, trobat resultats!</h3>";
    }
    mysqli_close($db_server);
}

?>

    </div>


    </div>
    <footer>
        <div><span>Copyright © 2018</span></div>
    </footer>

</body>

</html>
