<?php
function ConectaBD()
{
    //per inicialitzar la connexió amb base de dades.
    require_once 'login.php';
    $db_server = mysqli_connect($db_hostname, $db_username, $db_password, $db_database);
    if (!$db_server) {
        die("Connection failed: " . mysqli_connect_error());
    }
    //echo "Connected successfully<br>";
    $db_server->set_charset("utf8");
    return $db_server;
}

function BuscarToData($db_server, $data)
{
    if ($data != null) {
        $query = "SELECT COUNT(*) FROM seccio WHERE fecha = '$data'"; //creació de la query
        $result = mysqli_query($db_server, $query);
        if (!$result) {
            die("Database access failed: " . mysql_error());
        }
        //mostra el resultat.
        $consulta = mysqli_fetch_assoc($result);
        if ($consulta["COUNT(*)"] > 0) {
            $query = "SELECT hora FROM seccio WHERE fecha = '$data'"; //creació de la query
            $result = mysqli_query($db_server, $query);
            if (!$result) {
                die("Database access failed: " . mysql_error());
            }
            //mostra el resultat.
            $consulta = mysqli_fetch_assoc($result);
            $PeliHora = $consulta["hora"];

            //selecciona el seccio de la data indicada.
            $query = "SELECT nomPeli FROM seccio WHERE fecha = '$data'"; //creació de la query
            $result = mysqli_query($db_server, $query);
            if (!$result) {
                die("Database access failed: " . mysql_error());
            }

            $rows = mysqli_num_rows($result);

            //si hi ha mes de un peli per dia
            $codi_html = "";
            for ($i = 0; $i < $rows; $i++) {
                //mostra el resultat.
                $consulta = mysqli_fetch_assoc($result);
                foreach ($consulta as $key => $valor) {
                    $nomPeli = $valor;
                    $query = "SELECT * FROM pelicula WHERE nomPeli = '$nomPeli'"; //creació de la query

                    $result2 = mysqli_query($db_server, $query);
                    if (!$result2) {
                        die("Database access failed: " . mysql_error());
                    }
                    //mostra el resultat.

                    $rows2 = mysqli_num_rows($result2);
                    
                    for ($j = 0; $j < $rows2; $j++) {
                        $codi_html .= '<div class="item">';

                        $consulta2 = mysqli_fetch_assoc($result2);
                        foreach ($consulta2 as $key => $valor) {
                            if ($key == "nomPeli") {
                                $nomPeli = $valor;
                                $div_titol = "<div class='text_conten'>
                    <a href='php/pelicula.php?nomPeli=$nomPeli&fecha=$data&hora=$PeliHora'>
                    <h3>$valor</h3>
                    </a>
                    </div>";
                            } else if ($key == "imgUrl") {
                                $div_img = "<div class='img_conten'>
                    <a href='php/pelicula.php?nomPeli=$nomPeli&fecha=$data&hora=$PeliHora'>
                    <img class='key_img' src='$valor' alt='photo'>
                    </a>
                    </div>";
                            }
                        }

                      
                        $codi_html .= $div_img;
                        $codi_html .= $div_titol;
                        $codi_html .= "<div class='text_data_conten'><h3>" . $data ."</h3> </div>";
                        $codi_html .= '</div>';
                    }

                }
            }
        } else {
            $codi_html = "<h3>No, hi ha pelicules en el dia seleccionat!</h3>";
            return null;
        }

        return $codi_html;
    }
}
