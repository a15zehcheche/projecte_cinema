<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="js/jquery.js"></script>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/skeleton.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/homeFunction.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:200,200i,300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">


    <title>CINEMA</title>
</head>

<body>
    <header>
        <div class="conten">
            <div class="logo">
                <img src="img/IAM_CAT_logos.png" alt="imatge">
            </div>
            <div class="menu">
                <a href="index.php" class="crumb">Home</a>
                <a href="php/consulta.php">Consulta</a>
                <a href="php/buscar.php">Buscar</a>
            </div>
            <div class="AdministradorBox">
                 <?php 
                    session_start();

                    if ($_SERVER['REQUEST_METHOD'] == 'GET'){
                        if($_GET["action"] == "logout"){
                         unset($_SESSION['user_email']);
                         session_destroy();
                        }
                     }

                    if( $_SESSION['user_email'] !=null){
                        $nomUsuari = $_SESSION['user_email'];
                    echo '<a class="Administrador" href="./index.php?action=logout"><button title="ausias / ausias" type="button">Exit</button></a>';
                    echo "<a class='Administrador' href='php/login_user.php'><button title='UserName' type='button'>$nomUsuari</button></a>";
                    }else{
                        echo '<a class="Administrador" href="admin/"><button title="ausias / ausias" type="button">Administrador</button></a>';
                        echo ' <a class="Administrador" href="php/login_user.php"><button title="login" type="button">Login</button></a>';
                    }
                ?> 
                
            </div>
        </div>
    </header>

    <div class="marginCos">
        <h1 class="titol">CINEMA</h1>
        <div class="lineaSeparador"></div>
        <div class="show_box">
            <div class="img_box">
                <div class="left_icon">
                    <div class="icon_content">
                        <div class="icon_img"><img src="img/last.png"></div>
                    </div>
                </div>
                <div class="right_icon">
                    <div class="icon_content">
                        <div class="icon_img"><img src="img/next.png"></div>
                    </div>
                </div>
                <a href="php/pelicula.php?nomPeli=El Caballero Oscuro">
                    <img src="img/Portada_batman.jpg" alt="1">
                </a>
            </div>
            <div class="img_box">
                <div class="left_icon">
                    <div class="icon_content">
                        <div class="icon_img"><img src="img/last.png"></div>
                    </div>
                </div>
                <div class="right_icon">
                    <div class="icon_content">
                        <div class="icon_img"><img src="img/next.png"></div>
                    </div>
                </div>
                <a href="php/pelicula.php?nomPeli=La Llista de Schindler">
                    <img src="img/Portada_schindlers.jpg" alt="1">
                </a>
            </div>

            <div class="img_box">
                <div class="left_icon">
                    <div class="icon_content">
                        <div class="icon_img"><img src="img/last.png"></div>
                    </div>
                </div>
                <div class="right_icon">
                    <div class="icon_content">
                        <div class="icon_img"><img src="img/next.png"></div>
                    </div>
                </div>
                <a href="php/pelicula.php?nomPeli=Cadena perpètua">
                    <img src="img/Portada_shawshank.jpg" alt="1">
                </a>
            </div>
        </div>

        <div class="pelicula">
        <?php
                require_once 'php/function.php';
                $db_server = ConectaBD();

                $data = date("Y-m-d");

                //consultar numbre de seccions futures
                $query = "SELECT COUNT(*) FROM seccio WHERE fecha >= '$data'"; 
                $result = mysqli_query($db_server, $query);
                if (!$result) {
                    die("Database access failed: " . mysql_error());
                }
                $consulta = mysqli_fetch_assoc($result);
                $numPeli = $consulta["COUNT(*)"];
                if($numPeli>=4){
                    $mostra_numbre_pelis = 4;
                }else {
                    $mostra_numbre_pelis = $numPeli;
                }
               
                
                $count = 0;
                while($count < $mostra_numbre_pelis){
                    $codi_html_item = BuscarToData($db_server, $data);
                    if( $codi_html_item != NUll){
                        echo $codi_html_item;
                        $count++;
                    }
                    $data = date('Y-m-d', strtotime($data . ' +1 day'));

                }
                //buscar els prosims 4 pelicules.
                //echo BuscarToData($db_server, $_POST["data"]);
                mysqli_close($db_server);
            ?>
        </div>

    </div>
    <br>
    <footer>
        <div><span>Copyright © 2018</span></div>
    </footer>

</body>

</html>
