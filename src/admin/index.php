<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="../js/jquery.js"></script>
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/skeleton.css">
    <link rel="stylesheet" href="../css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:200,200i,300,300i,400,400i,600,600i,700,700i,900"
        rel="stylesheet">
    <link rel="stylesheet" href="../css/admin.css">
    <script src="../js/homeFunction.js"></script>
    <script src="../js/Chart.js"></script>
    <script src="../js/admin.js"></script>


    <title>Administrador</title>
</head>

<body>
    <header>
        <div class="conten">
            <div class="logo">
                <img src="../img/IAM_CAT_logos.png" alt="imatge">
            </div>
            <div class="menu">
                <a href="../index.php">Home</a>
                <a href="../php/consulta.php">Consulta</a>
                <a href="../php/buscar.php">Buscar</a>
            </div>
        </div>
    </header>
    <div class="marginCos">
        <h1 class="titol">Administrador</h1>
        <div class="lineaSeparador"></div>
        <div class="espectador"></div>
        <div class="elegir_fecha">
            <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="POST">
                <input type="date" name="data" placeholder="Introdueix una Data">
                <input type="submit" value="Buscar">
            </form>
        </div>
        <div class="llegenda">
            <p><span><img src="../img/disponible.png">Disponibles</span><span><img src="../img/lliurevip.png">Disponibles Vips</span><span><img src="../img/escollida.png">Escollides</span><span><img src="../img/ocupat.png">Ocupades</span></p>
        </div>
        <div class="mostra_butaques"></div>
        
        <div class="content">
            <div class="izquierda">
                <canvas id="grafic1" width="200" height="200"></canvas>
            </div>
            <div class="derecha">
                <div class="grafic_normal">
                    <canvas id="grafic2"></canvas>
                </div>
                <div class="grafic_vip">
                    <canvas id="grafic3"></canvas>
                </div>
            </div>
            <div class="Mostrar_preu"></div>
            </div>
        </div>
    <?php
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $date=$_POST['data'];
            }
            else{
                $date=date("Y/m/d");
            }
            //INICI CONNECXIO DB
            require "../php/login.php";
            $db_server = mysqli_connect($db_hostname,$db_username, $db_password,$db_database);
            if (!$db_server) {
                die("Connection failed: " . mysqli_connect_error());
            }
            $db_server->set_charset("utf8");
            $sql = "USE $db_database";
            mysqli_query($db_server, $sql);

            //INICI GETDATA
            echo "<input type='date' class='no_display date' name='data' value='$date'>";
            //INICI GETPREU
            $sql = "SELECT preu_base FROM pelicula";
            $result = mysqli_query($db_server, $sql);
            $consulta = mysqli_fetch_assoc($result);
            $preu = $consulta['preu_base'];
            if(date("w",strtotime($date))==3){
                $preu-=2;
                echo "<p class='no_display preu'>$preu</p>";
            }
            else{
                echo "<p class='no_display preu'>$preu</p>";
            }
            
            //INICI GETFILA
            $sql = "SELECT idSala FROM seccio WHERE fecha='$date'";
            $result = mysqli_query($db_server, $sql);
            $consulta = mysqli_fetch_assoc($result);
            $idSala = $consulta['idSala'];
            $sql = "SELECT filaVip FROM sala WHERE idSala='$idSala'";
            $result = mysqli_query($db_server, $sql);
            $consulta = mysqli_fetch_assoc($result);
            $filaVip = $consulta['filaVip'];
            echo "<p class='no_display fila'>$filaVip</p>";

            //INICI GETBUTAQUES_OCUPATs
            $butcas_ocupat = array();
            $sql = "SELECT codi_entrada FROM entrada WHERE fecha='$date'";
            $result = mysqli_query($db_server, $sql);
            $rows = mysqli_num_rows($result);
            for ($i = 0 ; $i < $rows ; $i++)
            {
                $consulta = mysqli_fetch_assoc($result);
                foreach ($consulta as $key => $valor){
                    $sql2 = "SELECT num_butaques FROM butaquesSeleccionat WHERE codi_entrada=$valor";
                    $result2 = mysqli_query($db_server, $sql2);
                    $rows2 = mysqli_num_rows($result2);
                    for ($j = 0 ; $j < $rows2 ; $j++)
                    {
                        $consulta2 = mysqli_fetch_assoc($result2);
                        foreach ($consulta2 as $key2 => $valor2){
                            array_push($butcas_ocupat,$valor2);
                        }
                        
                    }
                }
            }
            $count = count($butcas_ocupat);
            echo "<p class='no_display butacas_ocupat'>";
            foreach ($butcas_ocupat as $key => $valor){
                echo $valor." ";
            }
            echo "</p>";
            mysqli_close ($db_server);
        ?>
    <footer>
        <div><span>Copyright © 2018</span></div>
    </footer>

</body>

</html>